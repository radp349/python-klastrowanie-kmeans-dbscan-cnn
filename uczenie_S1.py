import numpy as np
from keras import Sequential
from keras.layers import Dense


def get_S1(path):
    x = []
    nr = []
    try:
        with open(path, "r", encoding='utf-8') as f:
            data = f.readlines()
        for line in data:
            words = line.split()
            x.append([float(words[0]), float(words[1])])
            nr.append(float(words[2]))
    except ValueError as err:
        print(path, "error")
        print(err)
    return x, nr


def get_X_y(data, size):
    data = data.T
    np.random.shuffle(data)
    X = []
    y = []
    for features, label in data:
        X.append(features)
        y.append(label)
    X = np.array(X).reshape(size, 2)
    y = np.array(y).reshape(size, 1)
    X = X / X.max()
    return X, y

training_data = np.array(get_S1("./S1_uczenie.txt"))
X_train = []
y_train = []
X_train, y_train = get_X_y(training_data, 4000)
with open("SOM_X_train_S1.txt", "w", encoding='utf-8') as f:
    for numer in range(len(X_train)):
        f.write(str(X_train[numer][0]))
        f.write(" ")
        f.write(str(X_train[numer][1]))
        f.write(" ")
        f.write(str(int(y_train[numer])))
        f.write("\n")

val_data = np.array(get_S1("./S1_weryf.txt"))
X_val = []
y_val = []
X_val, y_val = get_X_y(val_data, 500)

model = Sequential()
model.add(Dense(4, input_dim=2, activation='relu'))
model.add(Dense(15, activation='softmax'))
model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit(X_train, y_train, validation_data=(X_val, y_val), epochs=128, batch_size=64)
model.save("CNN_model_2w.h5")
