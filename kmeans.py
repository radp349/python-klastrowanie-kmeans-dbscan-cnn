import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

colors = [(0.0, 0.0, 0.0),(1.0, 0.0, 0.0),(0.0, 1.0, 0.1),(0.1, 0.0, 1.0),(0.4, 0.0, 0.6),(0.0, 0.6, 0.7),(0.6, 0.0, 0.8),(0.7, 0.8, 0.0),
          (0.0, 0.3, 0.6),(0.2, 0.5, 0.8),(0.5, 1.0, 0.4),(0.2, 0.0, 0.9),(1.0, 0.7, 0.0),(0.9, 0.3, 0.5),(0.5, 0.0, 0.8)]


X = []
x=[]
y=[]
with open('s1.txt') as data:
    for line in data:
        cell = line.split()
        cell[0] = int(cell[0])
        cell[1] = int(cell[1])
        x.append(cell[0])
        y.append(cell[1])
        X.append(cell)


def kmeans2(X, k):
  cluster = np.zeros(len(X))
  centroids = [[900000,750000], [800000,300000], [200000,400000], [600000,300000],[500000,400000],[500000,700000],[600000,600000],[700000,800000],[1000000,0],
               [900000,900000],[345678,398765],[156284,156284],[123456,654321],[456789,987654],[123789,987321]]
  start_cen = [[900000,750000], [800000,300000], [200000,400000], [600000,300000],[500000,400000],[500000,700000],[600000,600000],[700000,800000],[1000000,0],
               [900000,900000],[345678,398765],[156284,156284],[123456,654321],[456789,987654],[123789,987321]]
  necessary = True
  while necessary:
     for i in range (len(X)):
         mn_dist = float('inf')
         for c in range(len(centroids)):
            d = np.sqrt((centroids[c][0]-X[i][0])**2 + (centroids[c][1]-X[i][1])**2)
            if mn_dist > d:
               mn_dist = d
               cluster[i] = c
               
     new = np.zeros((k,2))
     div = np.zeros(k)
     for index in range(len(X)):
         new[int(cluster[index])][0] += X[index][0]
         new[int(cluster[index])][1] += X[index][1]
         div[int(cluster[index])] += 1 
         
     for c in range(len(new)):
         if div[c]!=0:
            new[c][0] /= div[c]
            new[c][1] /= div[c]
            
     
     sub = centroids-new
     nonzero = 0
     for row in sub:
         for value in row:
             if value != 0:
                 nonzero+=1
     if nonzero == 0:      
        necessary = False
     else:
        centroids = new
  return centroids, cluster, start_cen

k = 15
centroids, cluster, start_cen = kmeans2(X, k)

sx=[]
sy=[]
for abc in start_cen:
    sx.append(abc[0])
    sy.append(abc[1])

plt.scatter(x, y, c=cluster)
plt.scatter(centroids[:,0], centroids[:, 1], s=400, c=colors, marker='x')
plt.scatter(sx, sy, s=400, c=colors, marker='o')
plt.xlabel('X')
plt.ylabel('Y')
plt.show()


# ---------------------------------------9 WYMIARÓW---------------------------------------------------------------
colors = [(1.0, 0.1, 0.0),(0.1, 0.0, 0.1)]

X = []
zero=[]
jeden=[]
dwa=[]
trzy=[]
cztery=[]
piec=[]
szesc=[]
siedem=[]
osiem=[]

with open('breast.txt') as data:
    for line in data:
        cell = line.split()
        cell[0] = int(cell[0])
        cell[1] = int(cell[1])
        cell[2] = int(cell[2])
        cell[3] = int(cell[3])
        cell[4] = int(cell[4])
        cell[5] = int(cell[5])
        cell[6] = int(cell[6])
        cell[7] = int(cell[7])
        cell[8] = int(cell[8])

        zero.append(cell[0])
        jeden.append(cell[1])
        dwa.append(cell[2])
        trzy.append(cell[3])
        cztery.append(cell[4])
        piec.append(cell[5])
        szesc.append(cell[6])
        siedem.append(cell[7])
        osiem.append(cell[8])
        X.append(cell)


def kmeans9(X, k):
  cluster = np.zeros(len(X))
  centroids = [[0,1,2,3,4,5,6,7,8],[9,8,7,6,5,4,3,2,1]]
  start_cen = [[0,1,2,3,4,5,6,7,8],[9,8,7,6,5,4,3,2,1]]
  necessary = True
  while necessary:
     for i in range (len(X)):
         mn_dist = float('inf')
         for c in range(len(centroids)):
            d = np.sqrt( (centroids[c][0]-X[i][0])**2 + (centroids[c][1]-X[i][1])**2  + (centroids[c][2]-X[i][2])**2  + (centroids[c][3]-X[i][3])**2
                     + (centroids[c][4]-X[i][4])**2 + (centroids[c][5]-X[i][5])**2 + (centroids[c][6]-X[i][6])**2 + (centroids[c][7]-X[i][7])**2
                     + (centroids[c][8]-X[i][8])**2 )
            if mn_dist > d:
               mn_dist = d
               cluster[i] = c
               
     new = np.zeros((k,9))
     div = np.zeros(k)
     for index in range(len(X)):
         new[int(cluster[index])][0] += X[index][0]
         new[int(cluster[index])][1] += X[index][1]
         new[int(cluster[index])][2] += X[index][2]
         new[int(cluster[index])][3] += X[index][3]
         new[int(cluster[index])][4] += X[index][4]
         new[int(cluster[index])][5] += X[index][5]
         new[int(cluster[index])][6] += X[index][6]
         new[int(cluster[index])][7] += X[index][7]
         new[int(cluster[index])][8] += X[index][8]
         div[int(cluster[index])] += 1 
         
     for c in range(len(new)):
         if div[c]!=0:
            new[c][0] /= div[c]
            new[c][1] /= div[c]
            new[c][2] /= div[c]
            new[c][3] /= div[c]
            new[c][4] /= div[c]
            new[c][5] /= div[c]
            new[c][6] /= div[c]
            new[c][7] /= div[c]
            new[c][8] /= div[c]

      
     sub = centroids-new
     nonzero = 0
     for row in sub:
         for value in row:
             if value != 0:
                 nonzero+=1
     if nonzero == 0:      
        necessary = False
     else:
        centroids = new
  return centroids, cluster, start_cen

k = 2
centroids, cluster, start_cen = kmeans9(X, k)

s0=[]
s1=[]
s2=[]
s3=[]
s4=[]
s5=[]
s6=[]
s7=[]
s8=[]

for abc in start_cen:
    s0.append(abc[0])
    s1.append(abc[1])
    s2.append(abc[2])
    s3.append(abc[3])
    s4.append(abc[4])
    s5.append(abc[5])
    s6.append(abc[6])
    s7.append(abc[7])
    s8.append(abc[8])

fig = plt.figure()
ax = Axes3D(fig)

ax.scatter(zero, jeden, dwa, c=cluster)
ax.scatter(centroids[:,0], centroids[:, 1], centroids[:, 2], s=400, c=colors, marker='x')
ax.scatter(s0, s1, s2, s=400, c=colors, marker='o')
plt.xlabel('X')
plt.ylabel('Y')
plt.ylabel('Z')
plt.show()

fig = plt.figure()
ax = Axes3D(fig)

ax.scatter(trzy, cztery, piec, c=cluster)
ax.scatter(centroids[:,3], centroids[:, 4], centroids[:, 5], s=400, c=colors, marker='x')
ax.scatter(s3, s4, s5, s=400, c=colors, marker='o')
plt.xlabel('X')
plt.ylabel('Y')
plt.ylabel('Z')
plt.show()

fig = plt.figure()
ax = Axes3D(fig)

ax.scatter(szesc, siedem, osiem, c=cluster)
ax.scatter(centroids[:,6], centroids[:, 7], centroids[:, 8], s=400, c=colors, marker='x')
ax.scatter(s6, s7, s8, s=400, c=colors, marker='o')
plt.xlabel('X')
plt.ylabel('Y')
plt.ylabel('Z')
plt.show()