import numpy as np
import matplotlib.pyplot as plt
from keras.models import load_model
import susi


def get_S1(path):
    x = []
    y = []
    nr = []
    try:
        with open(path, "r", encoding='utf-8') as f:
            data = f.readlines()
        for line in data:
            words = line.split()
            x.append(int(words[0]))
            y.append(int(words[1]))
            nr.append(int(words[2]))
    except ValueError as err:
        print(path, "error")
        print(err)
    return x, y, nr


dane_testowe = np.array(get_S1("./S1_test.txt"))
dane_testowe = dane_testowe.T
np.random.shuffle(dane_testowe)

X_test = dane_testowe[:, 0:2]
y_test = dane_testowe[:, 2]

############################################################    CNN
model = load_model("CNN_model_2w.h5")
cnn_out = model.predict(X_test / X_test.max())
_, accuracy = model.evaluate(X_test / X_test.max(), y_test)
print("CNN Accuracy: ", accuracy * 100)

kolor = ["#990033", "#660099", "#0033ff", "#000033", "#006633", "#339933", "Yellow", "red", "#FF0000", "#A8A8A8", "#FF1CAE", "#8C1717", "#ff6600", "Lime", "#66ccff"]

cnn_out = list(cnn_out)
cnn_out_nr_centra = [x.argmax() for x in cnn_out]   #przypisanie numeru centrum do poszczególnych punktów z CNN
############################################################    SOM
X_train = []
y_train = []
with open("SOM_X_train_S1.txt", "r", encoding='utf-8') as f:   # wczytuje te same dane uczące które były wykorzystane do CNN
    data = f.readlines()
    for line in data:
        words = line.split()
        X_train.append(float(words[0]))
        X_train.append(float(words[1]))
        y_train.append(float(words[2]))
X_train = np.array(X_train).reshape(4000, 2)
som = susi.SOMClassifier()
som.fit(X_train, y_train)
som_out = som.predict(X_test / X_test.max())
accuracy = som.score(X_test / X_test.max(), y_test)
print("SOM Accuracy: ", accuracy * 100)

som_out = list(som_out)
X_test_list = X_test.tolist()
X_test_list = np.array(X_test_list)
############################################################    rysowanie wykresów

fig, axs = plt.subplots(1,3)

for dana in dane_testowe:
    axs[0].plot(dana[0], dana[1], color=kolor[dana[2]],marker='o')
axs[0].set_title("2 wymiary: K-Means")


for numer in range(len(dane_testowe)):
    axs[1].plot(dane_testowe[numer][0], dane_testowe[numer][1], color=kolor[cnn_out_nr_centra[numer]],marker='o')
axs[1].set_title("2 wymiary: CNN")


for numer in range(len(dane_testowe)):
    axs[2].plot(dane_testowe[numer][0], dane_testowe[numer][1], color=kolor[int(som_out[numer])],marker='o')
axs[2].set_title("2 wymiary: SOM")
plt.show()

