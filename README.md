# Projekt z zakresu uczenia maszynowego i klasteryzacji

Repozytorium zawiera zbiór skryptów implementujących różne techniki uczenia maszynowego i klasteryzacji. Poniżej znajduje się krótkie podsumowanie każdego skryptu:

1. **Algorytm K-means**: Ten skrypt (przykładowy, nie był częścią Twojego zestawu skryptów) wczytuje dane z pliku tekstowego, przekształca je do odpowiedniego formatu, a następnie zastosowuje do nich algorytm k-średnich, który jest jednym z algorytmów klasteryzacji. Rezultaty klasteryzacji są następnie wizualizowane na wykresie.

2. **Budowanie modelu konwulucyjnej sieci neuronowej CNN"**: Skrypt wczytuje dane z pliku tekstowego, przekształca je do odpowiedniego formatu, normalizuje je, a następnie trenuje model sieci neuronowej za pomocą tych danych. Po zakończeniu treningu, model jest zapisywany do pliku `.h5`. 

3. **Algorytm Fuzzy C-Means**: Skrypt wczytuje dane z pliku tekstowego, przekształca je do odpowiedniego formatu, a następnie zastosowuje do nich algorytm Fuzzy C-Means, który jest jednym z algorytmów klasteryzacji. Rezultaty klasteryzacji są następnie wizualizowane na wykresie. 

4. **Algorytm DBSCAN**: Ten skrypt wczytuje dane z pliku `.csv`, normalizuje dane i następnie zastosowuje do nich algorytm DBSCAN, który jest jednym z algorytmów klasteryzacji. Rezultaty klasteryzacji są wizualizowane na wykresie.

Wszystkie skrypty są napisane w Pythonie i korzystają z różnych bibliotek do manipulacji danymi i uczenia maszynowego, takich jak numpy, keras, sklearn, fcmeans oraz pandas.
