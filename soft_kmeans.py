import numpy as np
from fcmeans import FCM
from matplotlib import pyplot as plt

X=[]
with open('s1.txt') as data:
    for line in data:
        cell = line.split()
        cell[0] = int(cell[0])
        cell[1] = int(cell[1])
        X.append(cell)
X=np.array(X)

fcm = FCM(n_clusters=15)
fcm.fit(X)

# outputs
fcm_centers = fcm.centers
fcm_labels = fcm.predict(X)
print(len(fcm_labels))


plt.scatter(X[:,0], X[:,1], c=fcm_labels, alpha=1)
plt.scatter(fcm_centers[:,0], fcm_centers[:,1], marker="x", s=500, c='r')
plt.show()
