# ================================================================================================================
# ----------------------------------------------------------------------------------------------------------------
#									            DBSCAN
# ----------------------------------------------------------------------------------------------------------------
# ================================================================================================================

import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import queue
import pandas as pd
import math

# Szukam wszystkich sasiadów punku data[pointIndex] w odległości epsilon i zapisuję ich indeksy do listy points[]
def neighbour_points(data, pointIndex, epsilon):
    points = []
    
    for i in range(len(data)):      
        if( math.sqrt((data[i][0] - data[pointIndex][0])**2 + (data[i][1] - data[pointIndex][1])**2)  <= epsilon):
            points.append(i)
                 
    return points



# Funckja znajdująca klastry metodą DBSCAN
def fit(data, Eps, MinPt):

    # Na początku oznaczam wszystkie punkty jako szum
    point_label = [0] * len(data)
    point_count = []
    core = []
    border = []

    # Znajduję sąsiedztwo kazdego punktu w zbiorze
    for i in range(len(data)):
        point_count.append(neighbour_points(data, i, Eps))

    # Jeśli punkt ma odpowiednio dużo sąsiadów, to oznaczam go jako core
    for i in range(len(point_count)):
        if (len(point_count[i]) >= MinPt):
            point_label[i] = -1
            core.append(i)
        else:
            border.append(i)

    # Jeśli punkt nie miał odpowiednio dużo sąsiadów, ale znajduje się w sąsiedstwie punktu oznaczonego jako core, to oznaczam punkt jako border
    for i in border:
        for j in point_count[i]:
            if j in core:
                point_label[i] = -2
                break


    cluster = 1

    # Przypisuję punkty do klastrów
    for i in range(len(point_label)):
        q = queue.Queue()
        if (point_label[i] == -1):
            point_label[i] = cluster
            # Pierwszy punkt core oznaczam jako początek klastra, a następnie dodaję do niego również wszystkich sąsiadów tego punktu. 
            # Następnie dla sąsiadów również oznaczonych jako core, przeprowadzam identyczne działanie tak długo aż całe sąsiedztwo będzie już należeć do klastra.
            # Wówczas rozpoczynam oznaczanie następnych klastrów, które potrwa aż zabraknie punktów w zbiorze.
            for x in point_count[i]:
                if(point_label[x] == -1):
                    q.put(x)
                    point_label[x] = cluster
                elif(point_label[x] == -2):
                    point_label[x] = cluster
            while not q.empty():
                neighbors = point_count[q.get()]
                for y in neighbors:
                    if (point_label[y] == -1):
                        point_label[y] = cluster
                        q.put(y)
                    if (point_label[y] == -2):
                        point_label[y] = cluster
            cluster += 1  # Move on to the next cluster

    return point_label, cluster



# Funkcja do wyświetlania zbioru w oparciu o zidentyfikowane klastry
def visualize(data, cluster, numberOfClusters):
    N = len(data)

    colors = np.array(list(['#9933CC', '#3CA5D0', '#FF5B40', '#00CC33','#FF9900', '#666666']))

    for i in range(numberOfClusters):
        if (i == 0):
            color = '#000000'
        else:
            color = colors[i % len(colors)]
        x, y = [], []
        for j in range(N):
            if cluster[j] == i:
                x.append(data[j, 0])
                y.append(data[j, 1])
        plt.scatter(x, y, c=color, alpha=1, marker='.')
    plt.show()





# Wczytuję dataset
df = pd.read_csv("concentric_circles.csv")
# Zmnieniam jego formę na listę
dataset = df.astype(float).values.tolist()
# normalize dataset
X = StandardScaler().fit_transform(dataset)
point_labels, clusters = fit(X, 0.25, 4)


visualize(X, point_labels, clusters)

