import numpy as np
import matplotlib.pyplot as plt
import susi
from keras.models import load_model
from mpl_toolkits.mplot3d import Axes3D


def get_breast(path):
    x = []
    y = []
    z = []
    a = []
    b = []
    c = []
    j = []
    k = []
    l = []
    nr = []
    try:
        with open(path, "r", encoding='utf-8') as f:
            data = f.readlines()
        for line in data:
            words = line.split()
            x.append(int(words[0]))
            y.append(int(words[1]))
            z.append(int(words[2]))
            a.append(int(words[3]))
            b.append(int(words[4]))
            c.append(int(words[5]))
            j.append(int(words[6]))
            k.append(int(words[7]))
            l.append(int(words[8]))
            nr.append(float(words[9]))
    except ValueError as err:
        print(path, "error")
        print(err)
    return x, y, z, a, b, c, j, k, l, nr


dane_testowe = np.array(get_breast("./breast_test.txt"))
dane_testowe = dane_testowe.T
np.random.shuffle(dane_testowe)

X_test = dane_testowe[:, 0:9]
y_test = dane_testowe[:, 9]

############################################################    CNN
model = load_model("CNN_model_9w.h5")
cnn_out = model.predict(X_test / X_test.max())
_, accuracy = model.evaluate(X_test / X_test.max(), y_test)
print("CNN Accuracy: ", accuracy * 100)

kolor = ["black", "red"]

cnn_out = list(cnn_out)
cnn_out_nr_centra = [x.argmax() for x in cnn_out]   #przypisanie numeru centrum do poszczególnych punktów z CNN
############################################################    SOM
X_train = []
y_train = []
with open("SOM_X_train_breast.txt", "r", encoding='utf-8') as f:   # wczytuje te same dane uczące które były wykorzystane do CNN
    data = f.readlines()
    for line in data:
        words = line.split()
        X_train.append(float(words[0]))
        X_train.append(float(words[1]))
        X_train.append(float(words[2]))
        X_train.append(float(words[3]))
        X_train.append(float(words[4]))
        X_train.append(float(words[5]))
        X_train.append(float(words[6]))
        X_train.append(float(words[7]))
        X_train.append(float(words[8]))
        y_train.append(float(words[9]))
X_train = np.array(X_train).reshape(560, 9)
som = susi.SOMClassifier()
som.fit(X_train, y_train)
som_out = som.predict(X_test / X_test.max())
accuracy = som.score(X_test / X_test.max(), y_test)
print("SOM Accuracy: ", accuracy * 100)

som_out = list(som_out)
############################################################    rysowanie wykresów
fig = plt.figure(figsize=(8, 4), dpi=200)
plt.title(f"9 wymiarów: K-means")
ax = fig.add_subplot(131, projection='3d')
ay = fig.add_subplot(132, projection='3d')
az = fig.add_subplot(133, projection='3d')
for ctr in dane_testowe:
    ax.scatter(ctr[0], ctr[1], ctr[2], color=kolor[int(ctr[9])], s=40)
    ay.scatter(ctr[3], ctr[4], ctr[5], color=kolor[int(ctr[9])], s=40)
    az.scatter(ctr[6], ctr[7], ctr[8], color=kolor[int(ctr[9])], s=40)
plt.show()

fig = plt.figure(figsize=(8, 4), dpi=200)
plt.title(f"9 wymiarów: CNN")
ax = fig.add_subplot(131, projection='3d')
ay = fig.add_subplot(132, projection='3d')
az = fig.add_subplot(133, projection='3d')
for numer in range(len(dane_testowe)):
    ax.scatter(dane_testowe[numer][0], dane_testowe[numer][1], dane_testowe[numer][2], color=kolor[cnn_out_nr_centra[numer]], s=40)
    ay.scatter(dane_testowe[numer][3], dane_testowe[numer][4], dane_testowe[numer][5], color=kolor[cnn_out_nr_centra[numer]], s=40)
    az.scatter(dane_testowe[numer][6], dane_testowe[numer][7], dane_testowe[numer][8], color=kolor[cnn_out_nr_centra[numer]], s=40)
plt.show()

fig = plt.figure(figsize=(8, 4), dpi=200)
plt.title(f"9 wymiarów: SOM")
ax = fig.add_subplot(131, projection='3d')
ay = fig.add_subplot(132, projection='3d')
az = fig.add_subplot(133, projection='3d')
for numer in range(len(dane_testowe)):
    ax.scatter(dane_testowe[numer][0], dane_testowe[numer][1], dane_testowe[numer][2], color=kolor[int(som_out[numer])], s=40)
    ay.scatter(dane_testowe[numer][3], dane_testowe[numer][4], dane_testowe[numer][5], color=kolor[int(som_out[numer])], s=40)
    az.scatter(dane_testowe[numer][6], dane_testowe[numer][7], dane_testowe[numer][8], color=kolor[int(som_out[numer])], s=40)
plt.show()
