import numpy as np


def get_data(path, plik_S1):
    x = []
    y = []
    z = []
    a = []
    b = []
    c = []
    j = []
    k = []
    l = []
    nr = []
    try:
        with open(path, "r", encoding='utf-8') as f:
            data = f.readlines()
        if plik_S1:
            for line in data:
                words = line.split()
                x.append(int(words[0]))
                y.append(int(words[1]))
                nr.append(int(words[2]))
        else:
            for line in data:
                words = line.split()
                x.append(int(words[0]))
                y.append(int(words[1]))
                z.append(int(words[2]))
                a.append(int(words[3]))
                b.append(int(words[4]))
                c.append(int(words[5]))
                j.append(int(words[6]))
                k.append(int(words[7]))
                l.append(int(words[8]))
                nr.append(int(words[9]))
    except ValueError as err:
        print(path, "error")
        print(err)
    if plik_S1:
        return x, y, nr
    else:
        return x, y, z, a, b, c, j, k, l, nr


def write_txt(path, data, plik_S1):
    try:
        with open(path, "a", encoding='utf-8') as f:
            if plik_S1:
                f.write(str(data[0]))
                f.write(" ")
                f.write(str(data[1]))
                f.write(" ")
                f.write(str(data[2]))
                f.write("\n")
            else:
                f.write(str(data[0]))
                f.write(" ")
                f.write(str(data[1]))
                f.write(" ")
                f.write(str(data[2]))
                f.write(" ")
                f.write(str(data[3]))
                f.write(" ")
                f.write(str(data[4]))
                f.write(" ")
                f.write(str(data[5]))
                f.write(" ")
                f.write(str(data[6]))
                f.write(" ")
                f.write(str(data[7]))
                f.write(" ")
                f.write(str(data[8]))
                f.write(" ")
                f.write(str(data[9]))
                f.write("\n")
    except ValueError as err:
        print(path, "error")
        print(err)


def create_empty_file(string):
    with open(f"./{string}_uczenie.txt", "w", encoding='utf-8') as f:
        f.write("")
    with open(f"./{string}_weryf.txt", "w", encoding='utf-8') as f:
        f.write("")
    with open(f"./{string}_test.txt", "w", encoding='utf-8') as f:
        f.write("")


def dziel_dataset(file, plik_S1, string):
    for numer in range(len(file)):
        file[numer] = file[numer].T
        if numer < 0.8 * len(file):  # treningowe
            write_txt(f"./{string}_uczenie.txt", file[numer], plik_S1)
        elif numer < 0.9 * len(file):  # weryfikujące
            write_txt(f"./{string}_weryf.txt", file[numer], plik_S1)
        else:  # testowe
            write_txt(f"./{string}_test.txt", file[numer], plik_S1)


path_S1 = "./S1_numery.txt"
S1 = np.array(get_data(path_S1, True))
S1 = S1.T
np.random.shuffle(S1)
create_empty_file("S1")
dziel_dataset(S1, True, "S1")

path_breast = "./breast_numery.txt"
breast = np.array(get_data(path_breast, False))
breast = breast.T
np.random.shuffle(breast)
create_empty_file("breast")
dziel_dataset(breast, False, "breast")
